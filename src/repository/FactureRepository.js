import { ResultSetHeader, RowDataPacket } from "mysql2";
import { Facture } from "../entity/Facture";
import { User } from "../entity/User";
import { connection } from "./connection";
import { UserRepository } from "./UserRepository";

export class FactureRepository {

    static async findById(id){
        const [rows] = await connection.query(`SELECT * FROM facture WHERE id=?`, [id]);
        if (rows.length === 1) {
            let instance = new Facture(rows[0].label, rows[0].date,rows[0].description, rows[0].user_id, rows[0].id);
            return instance
        }
    }

    static async findAll() {
        const [rows] = await connection.query('SELECT * FROM facture');
        const factures = [];
        for(let row of rows) {
            factures.push(new Facture(row['label'], row['date'], row['description'], row['user_id'], row['id']));
        }
        return factures;
    }

    static async add(facture) {
        const [rows] = await connection.query<ResultSetHeader>(`INSERT into facture (label, date,
            description, user_id) VALUES (?, ?, ?, ?)`, [facture.label, facture.date, facture.description, facture.user_id, facture.id])
        facture.id = rows.insertId
    }

    
    static async findAllUser(idUser,withUser = false) {
        const [rows] = await connection.query("SELECT * FROM facture WHERE user_id = ?", [idUser]);
        const factures = [];
        for (const row of rows) {
            let facture = new Facture(rows[0].label, rows[0].date, rows[0].description, row.user_id, rows[0].id);
            facture.id = row['id'];
            facture.label = row['label'];
            facture.date = row['date'];
            facture.description = row['description'];

            if (withUser) {
                facture.user = await UserRepository.findById(row['user_id']);
            }

            factures.push(facture);
        }
        return factures;
    }
}