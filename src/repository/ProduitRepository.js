import { ResultSetHeader, RowDataPacket } from 'mysql2';
import { Categorie } from '../entity/Categorie';
import { Produit } from '../entity/Produit';
import { CategorieRepository } from './CategorieRepository';
import { connection } from './connection';

export class ProduitRepository {

    static async findById(id) {
        const [rows] = await connection.query(`SELECT * FROM produit WHERE id=?`, [id]);
        if (rows.length === 1) {
            let instance = new Produit(rows[0].label, rows[0].type, rows[0].prix, rows[0].couleur, rows[0].ajout, rows[0].matiere, rows[0].categorie_id, rows[0].id);
            return instance
        }
    }
    
    // static async findByCategorie_id(categorie_id) {
    //     const [rows] = await connection.query(`SELECT * FROM produit WHERE categorie_id=?`, [categorie_id]);
    //     if (rows.length === 1) {
    //         let instance = new Produit(rows[0].label, rows[0].taille, rows[0].type, rows[0].prix, rows[0].couleur, rows[0].ajout, rows[0].categorie_id, rows[0].matiere, rows[0].id);
    //         return instance
    //     }
    // }

    static async findAll() {
        const [rows] = await connection.query(`SELECT * FROM produit p LEFT JOIN travail t ON p.id=t.id_produit `); //(`SELECT * FROM produit`)
        return rows.map(row => new Produit(row['label'], row['type'], 
        row['prix'], row['couleur'], row['ajout'], rows['matiere'], rows['categorie_id'], row['id']));
    }

    // static async add(produit) {
    //     const [rows] = await connection.query<ResultSetHeader>(`INSERT INTO produit (label, taille, type, prix, 
    //         couleur, ajout, matiere, categorie_id ) VALUES (?, ?, ?, ?, ?, ?, ?, ?)`, 
    //         [produit.label, produit.taille, produit.type, produit.prix, produit.couleur, produit.ajout, produit.matiere, produit.categorie_id])
    //     produit.id = rows.insertId
    // }

    static async add(produit) {
        const [rows] = await connection.query(`INSERT INTO produit (label, 
            couleur, matiere) VALUES (?, ?, ?)`, 
            [produit.label, produit.couleur, produit.matiere])
        produit.id = rows.insertId
        const [rowSecond] = await connection.query(`INSERT INTO travail (type,
            prix, ajout) VALUES (?, ?, ?)`, 
            [produit.type, produit.prix, produit.ajout])
            produit.id = rowSecond.insertId
    }

    static async delete(id) {
        await connection.query("DELETE FROM produit WHERE id= ?", [id])
    }

    static async update(modif) {
        await connection.query(`UPDATE produit SET label = ?, 
        type = ?, prix = ?, couleur = ?, ajout = ?, matiere = ?, categorie_id = ? WHERE id = ?`, [modif.label,
            modif.type, modif.prix, modif.couleur, modif.ajout, modif.matiere, modif.categorie_id, modif.id])
    }



    static async findAllCategorie(idCategorie, withCategorie = false) {
        const [rows] = await connection.query("SELECT * FROM produit WHERE categorie_id = ?", [idCategorie]);
        const produits = [];
        for (const row of rows) {
            let produit = new Produit(row.label, row.type, row.prix, row.couleur, row.ajout, row.categorie_id, row.matiere, row.id);
            produit.id = row['id'];
            produit.label = row['label'];
            //produit.taille = row['taille'];
            produit.type = row['type'];
            produit.prix = row['prix'];
            produit.couleur = row['couleur'];
            produit.ajout = row['ajout'];
            produit.matiere = row['matiere'];

            if (withCategorie) {
                //categorie gros problème
                produit.categorie = await CategorieRepository.findById(row['categorie_id']);
            }

            produits.push(produit);
        }
        return produits;
    }


    // static async findByOneCategorie(idCategorie  ,withProduit = false) {
    //     const [rows] = await connection.query("SELECT * FROM produit WHERE categorie_id = ?", [idCategorie]);
    //     const produit[] = [];
    //     for (const row of rows) {
    //         let categorie = new Produit();
    //         categorie.id = row['id'];
    //         categorie.label = row['label'];
    //         categorie.taille = row['taille'];
    //         categorie.type = row['type'];
    //         categorie.prix = row['prix'];
    //         categorie.couleur = row['couleur'];
    //         categorie.ajout = row['ajout'];

    //         if (withProduit) {
    //             // categorie.produit = await ProduitRepository.findByCategory(id);
    //             categorie.produit = await ProduitRepository.findById(row['id_produit']);
    //         }
            
    //         produit.push(categorie);
    //     }
    //     return produit;
    // }

    // static async findByOneProduct(withProduit = false) {
    //     const [rows] = await connection.query("SELECT * FROM produit");
    //     const categories: Categorie[] = [];
    //     for (const row of rows) {
    //         let categorie = new Categorie(row.label, row.id);
    //         categorie.id = row['id'];
    //         categorie.label = row['label'];

    //         if (withProduit) {
    //             categorie.produit = await CategorieRepository.findById(categorie.id);
    //         }

    //         categories.push(categorie);
    //     }
    //     return categories;
    // }

    // static async findByCategory(categorie_id) {
    //     const [rows] = await connection.query(`SELECT * FROM produit WHERE categorie_id =?`, [categorie_id]);
    //     // const produits[] = [];
    //     // const categories: Categorie[] = [];
    //     // if (rows.length === 1) {
    //     //     let instance = new Categorie(rows[0].label, rows[0].id);
    //     //     return instance
    //     // }

    //     for (const item of produits) {
    //         const produits[] = [];
    //         item.produits = await CategorieRepository.(id);
    //     }
    //     return produits;
    // }
}