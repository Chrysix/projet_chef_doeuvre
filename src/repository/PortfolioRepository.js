import { ResultSetHeader, RowDataPacket } from "mysql2";
import { Portfolio } from "../entity/Portfolio";
import { connection } from "./connection";


export class PortfolioRepository {

    static async findById(id) {
        const [rows] = await connection.query(`SELECT * FROM portfolio WHERE id=?`, [id]);
        if (rows.length === 1) {
            let instance = new Portfolio(rows[0].name, rows[0].image, rows[0].description, rows[0].id);
            return instance
        }
    }

    static async findAll() {                //<ResultSetHeader> pour INSERT
        const [rows] = await connection.query('SELECT * FROM portfolio');
        const portfolios = [];
        for (let row of rows) {
            portfolios.push(new Portfolio(row['name'], row['image'], row['description'], row['id']));
        }
        return portfolios;
    }
    
    static async add(portfolio) {
        const [rows] = await connection.query<ResultSetHeader>(`INSERT into portfolio (name, image, description, id) VALUES (?, ?, ?, ?)`, [portfolio.name, portfolio.image, portfolio.description, portfolio.id])
        portfolio.id = rows.insertId
    }

}

