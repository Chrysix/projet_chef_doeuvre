import { ResultSetHeader, RowDataPacket } from 'mysql2';
import { Categorie } from '../entity/Categorie';
import { Produit } from '../entity/Produit';
import { connection } from './connection';
import { ProduitRepository } from './ProduitRepository';

export class CategorieRepository {

    static async findById(id) {
        const [rows] = await connection.query(`SELECT * FROM categorie WHERE id=?`, [id]);
        if (rows.length === 1) {
            let instance = new Categorie(rows[0].label, rows[0].id);
            
            return instance
        }
    }

    
    static async findAll(){
        const [rows] = await connection.query('SELECT * FROM categorie');
        return rows.map(row => new Categorie(row['label'], row['id']));
    }

    static async add(categorie) {
        const [rows] = await connection.query<ResultSetHeader>(`INSERT into categorie (label) VALUES (?)`, [categorie.label])
        categorie.id = rows.insertId
    }

    
    // static async findByOneProduct(categorie_id: number, withCategorie){
    //     const [rows] = await connection.query("SELECT * FROM produit WHERE categorie_id = ?", [categorie_id]);
    //     let item:any = [];

    //     for (const row of rows){
    //     let categorie = new Categorie(rows[0].label, rows[0].id);
    //     categorie.id = row['id'];
    //     categorie.label = row['label']
    //     if(withCategorie){
    //         item = await ProduitRepository.findById(row['categorie_id']);
    //     }
    //     item.push(categorie)
    //     }
    //     return item;
    // }

    // static async findAll(withAddresses = false) {
    //     const [rows] = await connection.query("SELECT * FROM person");
    //     const persons: Person[] = [];
    //     for (const row of rows) {
    //         let person = new Person();
    //         person.id = row['id'];
    //         person.name = row['name'];

    //         if (withAddresses) {
    //             person.addresses = await AddressRepository.findByPerson(person.id);
    //         }
            
    //         persons.push(person);
    //     }
    //     return persons;
    // }







    // static async findByOneProduct(id ,withProduit = false) {
    //     const [rows] = await connection.query("SELECT * FROM produit WHERE id = ?", [id]);
    //     const categories[] = [];
    //     for (const row of rows) {
    //         let categorie = new Categorie(row.label, row.id);

    //         if (withProduit) {
    //             categorie.produit = await ProduitRepository.findByCategory(id);
    //         }
            
    //         categories.push(categorie);
    //     }
    //     return categories;
    // }



    static async findByProduct(idProduit ,withProduit = false) {
        const [rows] = await connection.query("SELECT * FROM produit WHERE id = ?", [idProduit]);
        const categories = [];
        for (const row of rows) {
            let categorie = new Categorie(row.label, row.id);
            categorie.label = row['street']

            if (withProduit) {
                // categorie.produit = await ProduitRepository.findByCategory(id);
                categorie.produit = await ProduitRepository.findAllCategorie(row['id_produit']);
            }
            
            categories.push(categorie);
        }
        return categories;
    }


}