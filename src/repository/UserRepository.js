import { ResultSetHeader, RowDataPacket } from "mysql2";
import { User } from "../entity/User";
import { connection } from "./connection";
import { FactureRepository } from "./FactureRepository";

export class UserRepository{


    static async findById(id){
        const [rows] = await connection.query(`SELECT * FROM user WHERE id=?`, [id]);
        if (rows.length === 1) {
            let instance = new User(rows[0].name, rows[0].first_name, rows[0].email, rows[0].password, rows[0].adress, rows[0].code_postal, 
                rows[0].city, rows[0].id);
            return instance
        }
    }

    static async findAll(){
        const [rows] = await connection.query('SELECT * FROM user');
        return rows.map(row => new User(row['name'], row['first_name'], row['email'], row['password'], row['adress'], row['code_postal'], 
        row['city'], row['id']));
    }

    static async register(regist){
        const [rows] = await connection.query<ResultSetHeader>(`INSERT into user (name, first_name, email, password, adress, code_postal, city) 
        VALUES (?, ?, ?, ?, ?, ?, ?)`, 
        [regist.name, regist.first_name, regist.email, regist.password, regist.adress, regist.code_postal, regist.city])
        regist.id = rows.insertId;
    };

    static async findByEmail(email){
        const [rows] = await connection.query('SELECT * FROM user WHERE email=?', [email])
        if (rows.length === 1) {
            return new User(rows[0].name, rows[0].first_name, rows[0].email, rows[0].password, rows[0].adress, rows[0].code_postal, 
                rows[0].city, rows[0].id);
        };
        return null;
    };

    static async findByFacture(idFacture ,withFacture = false) {
        const [rows] = await connection.query("SELECT * FROM user WHERE id = ?", [idFacture]);
        const users = [];
        for (const row of rows) {
            let user = new User(row.name, row.first_name, row.email, row.password, row.adress, row.code_postal, row.city, row.id);
            user.name = row['name'];
            user.first_name = row['first_name'];
            user.email = row['email'];
            user.password = row['password'];
            user.adress = row['adress'];
            user.code_postal = row['code_postal'];
            user.city = row['city'];

            if (withFacture) {
                // facture.facture = await factureRepository.findByCategory(id);
                user.facture = await FactureRepository.findAllUser(row['id_facture']);
            }
            
            users.push(user);
        }
        return users;
    }
}