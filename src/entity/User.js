import { Facture } from "./Facture";

export class User{

    id;
    name;
    first_name;
    email;
    password;
    adress;
    code_postal;
    city;
    facture;

    constructor(name = null, first_name = null, email = null, password = null, adress = null, code_postal = null, city = null, id = null){
        this.name = name
        this.first_name = first_name
        this.email = email
        this.password = password
        this.adress = adress
        this.code_postal = code_postal
        this.city = city
        this.id = id
    }
}