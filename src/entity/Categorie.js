import { Produit } from "./Produit";

export class Categorie{

    id;
    label;
    produit;

    constructor(label, id){
        this.label = label
        this.id = id
    }
}