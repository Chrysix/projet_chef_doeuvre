import { Categorie } from "./Categorie";
import { Travail } from "./Travail";

export class Produit{

    id;
    label;
    //taille;
    couleur;
    matiere
    categorie_id;
    categorie;
    travail;
    

    constructor(label, taille, type, prix, 
        couleur, ajout,matiere, categorie_id, t, id){
            
        this.label = label
        //this.taille = taille
        this.couleur = couleur
        this.matiere = matiere
        this.categorie_id = categorie_id
        this.travail = t;
        this.id = id
    }
}