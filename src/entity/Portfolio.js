export class Portfolio{

    id;
    name;
    image;
    description;

    constructor(name, image, description, id){
        this.name = name
        this.image = image
        this.description = description
        this.id = id
    }
}