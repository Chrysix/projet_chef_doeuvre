import { User } from "./User";

export class Facture{

    id;
    label;
    date;
    description;
    user_id;
    user;

    constructor(label, date, description, user_id, id){
        this.label = label
        this.date = date
        this.description = description
        this.user_id = user_id
        this.id = id
    }
}