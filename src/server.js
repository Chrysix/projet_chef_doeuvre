import express from 'express';
import cors from 'cors';
import { user_controller } from "./controller/user_controller";
import { categorie_controller } from "./controller/categorie_controller";
import { portfolio_controller } from "./controller/portfolio_controller";
import { produit_controller } from "./controller/produit_controller";
import { facture_controller } from "./controller/facture_controller";
import { configurePassport } from "./utils/token";
import passport from "passport";

export const server = express();

server.use(passport.initialize());

server.use(express.json());
server.use(cors());

configurePassport()

server.use('/api/user',(user_controller));
server.use('/api/categorie',(categorie_controller));
server.use('/api/portfolio',(portfolio_controller));
server.use('/api/produit',(produit_controller));
server.use('/api/facture',(facture_controller));