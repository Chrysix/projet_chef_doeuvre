import { Router } from "express";
import { User } from "../entity/User";
import bcrypt from 'bcrypt';
import { UserRepository } from "../repository/UserRepository";
import {generateToken} from "../utils/token"
import passport from "passport";
import { FactureRepository } from "../repository/FactureRepository";

export const user_controller = Router()

user_controller.get("/", async (req, resp)  =>{
    try {
        const user = await UserRepository.findAll()
        resp.status(201).json(user)
        
    } catch (error) {
        console.log(error);
        resp.status(500).json(error);

    }
});

user_controller.get("/:id", async(req, resp) => {
    try {
        const item = await UserRepository.findById(req.params.id)
        item.facture = await FactureRepository.findAllUser(Number(req.params.id))
        resp.status(201).json(item)
        
    } catch (error) {
        console.log(error);
        resp.status(500).json(error);

    }
})

user_controller.post("/register", async (req, resp) =>{
    try {
        const newUser = new User();
        Object.assign(newUser, req.body);

        const exists = await UserRepository.findByEmail(
            newUser.email)

        if (exists) {
            resp.status(400).json({error: 'Email already token'})
            return;
        }

        newUser.password= await bcrypt.hash(
            newUser.password, 12)

        await UserRepository.register(newUser)
        resp.status(201).json(newUser)
    } catch (error) {
        console.log(error);
        resp.status(500).json(error)
    }
})

user_controller.post("/login", async (req, resp) => {
    try {
        const user = await UserRepository.findByEmail(
            req.body.email)
        if (user) {
            console.log(user, req.body.password);
            const samePassword = await bcrypt.compare(
                req.body.password, user.password);

            if (samePassword) {
                resp.json({
                    user,
                    token: generateToken(user)
                });
                return;
            }
        };
        resp.status(401).json({ error: 'Wrong email and/or email' })
    }
    catch (error) {
        console.log(error);
        resp.status(500).json(error)
    }
});


user_controller.get('/', passport.authenticate('jwt', { session: false }), (req, res) => {
    // res.json({ message: "hello " + req.user.email });
    res.json({message: '' + req.body});
});

// user_controller.get('/account', passport.authenticate('jwt', { session: false }), (req, res) => {
//     res.json(req.user);
// });

user_controller.get("/facture/:id", async(req, resp) => {
    try {
        const produit = await UserRepository.findByFacture(Number(req.params.id ))
        resp.status(201).json(produit)
        
    } catch (error) {
        console.log(error);
        resp.status(500).json(error);

    }
})