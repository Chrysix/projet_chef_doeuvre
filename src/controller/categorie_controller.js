import { Router } from "express"
import { CategorieRepository } from "../repository/CategorieRepository"
import { ProduitRepository } from "../repository/ProduitRepository"

export const categorie_controller = Router()


categorie_controller.get("/:id", async(req, resp) => {
    try {
        const topic = await CategorieRepository.findById(req.params.id)
        topic.produit = await ProduitRepository.findAllCategorie(Number(req.params.id))
        resp.status(201).json(topic)
        
    } catch (error) {
        console.log(error);
        resp.status(500).json(error);

    }
})


categorie_controller.get("/", async (req, resp)  =>{
    try {
        const topic = await CategorieRepository.findAll()
        resp.status(201).json(topic)
        
    } catch (error) {
        console.log(error);
        resp.status(500).json(error);

    }
})

categorie_controller.post("/", async (req, resp)  =>{ 
    try {
        const topic = await CategorieRepository.add(req.body)
        resp.status(201).json(topic)
        
    } catch (error) {
        console.log(error);
        resp.status(500).json(error);

    }
})

categorie_controller.get("/product/:id", async(req, resp) => {
    try {
        const produit = await CategorieRepository.findByProduct(Number(req.params.id ))
        resp.status(201).json(produit)
        
    } catch (error) {
        console.log(error);
        resp.status(500).json(error);

    }
})