import { Router } from "express";
import passport from "passport";
import { FactureRepository } from "../repository/FactureRepository";
import { ProduitRepository } from "../repository/ProduitRepository";
import { UserRepository } from "../repository/UserRepository";

export const facture_controller = Router()

facture_controller.get("/user/:user_id", async(req, resp) => {
    try {
        const facture = await FactureRepository.findAllUser(Number(req.params.user_id))
        resp.status(201).json(facture)
        
    } catch (error) {
        console.log(error);
        resp.status(500).json(error);

    }
})


facture_controller.get("/", async (req, resp)  =>{
    try {
        const item = await FactureRepository.findAll()
        resp.status(201).json(item)
        
    } catch (error) {
        console.log(error);
        resp.status(500).json(error);

    }
})

// facture_controller.post("/", async (req, resp)  =>{ 
//     try {
//         const item = await FactureRepository.add(req.body)
//         resp.status(201).json(item)

        
        
//     } catch (error) {
//         console.log(error);
//         resp.status(500).json(error);

//     }
//})

    facture_controller.post("/", passport.authenticate('jwt', { session: false }),async (req, res)  =>{ 
        try {
            const item = await FactureRepository.add(req.body)
            res.status(201).json(item)
            res.json(req.user);
            
            
        } catch (error) {
            console.log(error);
            res.status(500).json(error);
    
        }
    })

// user_controller.get('/account', passport.authenticate('jwt', { session: false }), (req, res) => {
//                 res.json(req.user);
//             });