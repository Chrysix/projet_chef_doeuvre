import { Router } from "express";
import { PortfolioRepository } from "../repository/PortfolioRepository";

export const portfolio_controller = Router()

portfolio_controller.get("/:id", async(req, resp) => {
    try {
        const item = await PortfolioRepository.findById(req.params.id)
        resp.status(201).json(item)
        
    } catch (error) {
        console.log(error);
        resp.status(500).json(error);

    }
})


portfolio_controller.get("/", async (req, resp)  =>{
    try {
        const item = await PortfolioRepository.findAll()
        resp.status(201).json(item)
        
    } catch (error) {
        console.log(error);
        resp.status(500).json(error);

    }
})


portfolio_controller.post("/", async (req, resp)  =>{ 
    try {
        const item = await PortfolioRepository.add(req.body)
        resp.status(201).json(item)
        
    } catch (error) {
        console.log(error);
        resp.status(500).json(error);

    }
})