import { Router } from "express";
import { ProduitRepository } from "../repository/ProduitRepository";

export const produit_controller = Router()

produit_controller.get("/:id", async(req, resp) => {
    try {
        const produit = await ProduitRepository.findById(req.params.id)
        resp.status(201).json(produit)
        
    } catch (error) {
        console.log(error);
        resp.status(500).json(error);

    }
})

produit_controller.get("/categorie/:categorie_id", async(req, resp) => {
    try {
        const produit = await ProduitRepository.findAllCategorie(Number(req.params.categorie_id))
        resp.status(201).json(produit)
        
    } catch (error) {
        console.log(error);
        resp.status(500).json(error);

    }
})


produit_controller.get("/", async (req, resp)  =>{
    try {
        const produit = await ProduitRepository.findAll()
        resp.status(201).json(produit)
        
    } catch (error) {
        console.log(error);
        resp.status(500).json(error);

    }
})

produit_controller.post("/", async (req, resp)  =>{ 
    try {
        const produit = await ProduitRepository.add(req.body)
        resp.status(201).json(produit)
        
    } catch (error) {
        console.log(error);
        resp.status(500).json(error);

    }
})

// produit_controller.get("/:id", async(req, resp) => {
//     try {
//         const produit = await ProduitRepository.findByOneProduct(req.body, req.params.id )
//         resp.status(201).json(produit)
        
//     } catch (error) {
//         console.log(error);
//         resp.status(500).json(error);

//     }
// })