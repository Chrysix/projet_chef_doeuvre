
USE projet_chef_doeuvre;

DROP TABLE user;
CREATE TABLE user(
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(256) NOT NULL,
    first_name VARCHAR(256) NOT NULL,
    email VARCHAR(256) NOT NULL,
    password VARCHAR(256) NOT NULL,
    adress VARCHAR(256) NOT NULL,
    code_postal VARCHAR(256) NOT NULL,
    city VARCHAR(256) NOT NULL
);

DROP TABLE portfolio;
CREATE TABLE portfolio(
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(256) NOT NULL,
    image VARCHAR(256) NOT NULL,
    description VARCHAR(256) NOT NULL
);

DROP TABLE produit;
CREATE TABLE produit(
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    label VARCHAR(256) NOT NULL,
    -- taille VARCHAR(256),
    -- type VARCHAR(256),
    -- prix INTEGER,
    couleur VARCHAR(256),
    -- ajout VARCHAR(256),
    matiere VARCHAR(256),
    categorie_id INTEGER,
    FOREIGN KEY (categorie_id) REFERENCES categorie(id)
    
);

DROP TABLE travail;
CREATE TABLE travail(
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    type VARCHAR(256),
    prix INTEGER,
    ajout VARCHAR(256),
    id_produit INTEGER
    -- categorie_id INTEGER,
    -- FOREIGN KEY (categorie_id) REFERENCES categorie(id)
    
);

DROP TABLE categorie;
CREATE TABLE categorie(
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    label VARCHAR(256) NOT NULL
);

DROP TABLE facture;
CREATE TABLE facture(
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    label VARCHAR(256) NOT NULL,
    date DATE NOT NULL,
    description VARCHAR(256) NOT NULL,
    user_id INTEGER,
    FOREIGN KEY (user_id) REFERENCES user(id)
);


SELECT * FROM produit LEFT JOIN categorie ON produit.categorie_id=categorie.id;


INSERT INTO produit (label, taille, type, prix, couleur, ajout, categorie_id) VALUES (?, ?, ?, ?, ?, ?, ?),`produit.label, produit.taille, produit.type, produit.prix, produit.couleur, produit.ajout, produit.categorie_id` LEFT JOIN categorie ON produit.categorie_id=categorie.id;



-- DROP TABLE produit;
-- CREATE TABLE produit(
--     id INTEGER PRIMARY KEY AUTO_INCREMENT,
--     label VARCHAR(256) NOT NULL,
--     taille VARCHAR(256),
--     -- type VARCHAR(256),
--     -- prix INTEGER,
--     couleur VARCHAR(256),
--     -- ajout VARCHAR(256),
--     matiere VARCHAR(256),
--     categorie_id INTEGER,
--     FOREIGN KEY (categorie_id) REFERENCES categorie(id)
    
-- );

-- DROP TABLE travail;
-- CREATE TABLE travail(
--     id INTEGER PRIMARY KEY AUTO_INCREMENT,
--     type VARCHAR(256),
--     prix INTEGER,
--     ajout VARCHAR(256),
--     id_produit INTEGER
--     -- categorie_id INTEGER,
--     -- FOREIGN KEY (categorie_id) REFERENCES categorie(id)
    
-- );